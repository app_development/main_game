import 'package:flutter/material.dart';

// iPhone 14 Pro Max height: 932.0
// iPhone 14 Pro Max width: 430.0

const double buttonBorderWidth = 3.0;

const double buttonBorderRadiusSize = 16.0;

const BorderRadius standardBorderRadius =
    BorderRadius.all(Radius.circular(buttonBorderRadiusSize));

const double maxWidthOfWidget = 600.0;

const double standardButtonmaxHeight = 115;

const double fixedBottomSizedBoxPadding = 12;
