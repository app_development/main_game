import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_vibrate/flutter_vibrate.dart';

import '../../controller/game_controller.dart';
import '../../controller/player_controller.dart';
import '../../model/player.dart';
import '../Buttons/textfield_start_button.dart';

// Text field should stay at the bottom but its always directly under the list
// Start Button should always has the same size (height as the Textfield)

class PlayerInput extends StatefulWidget {
  const PlayerInput({super.key});

  @override
  State<PlayerInput> createState() => _PlayerInputState();
}

class _PlayerInputState extends State<PlayerInput> {
  final FocusNode _playerInputFocusNode = FocusNode();
  final TextEditingController _playerInputController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _playerInputFocusNode.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    super.dispose();
    _playerInputFocusNode.removeListener(_onFocusChange);
    _playerInputFocusNode.dispose();
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    PlayerController playerController = Get.find();
    final OutlineInputBorder outlinedBorder = OutlineInputBorder(
        borderRadius: const BorderRadius.horizontal(left: Radius.circular(100)),
        borderSide:
            BorderSide(color: Theme.of(context).primaryColor, width: 2.5));
    final double screenHeight = MediaQuery.of(context).size.height;
    final double displayWidth = MediaQuery.of(context).size.width > 2000.0
        ? 2000.0
        : MediaQuery.of(context).size.width;

    double textFieldHeight = screenHeight * 0.072;

    return Padding(
      padding: _playerInputFocusNode.hasFocus
          ? EdgeInsets.fromLTRB(0, screenHeight * 0.0268, 0, 15.0)
          : EdgeInsets.fromLTRB(0, screenHeight * 0.0912, 0, 48),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: textFieldHeight,
            width: displayWidth * 0.697,
            child: TextField(
                maxLines: null,
                expands: true,
                //keyboardType: TextInputType.name,
                autofocus: true,
                controller: _playerInputController,
                focusNode: _playerInputFocusNode,
                style: TextStyle(
                  fontSize: 16,
                  height: 1.16,
                  color: Theme.of(context).primaryColor,
                ),
                textAlign: TextAlign.left,
                cursorHeight: 20.0,
                decoration: InputDecoration(
                    isDense: true,
                    suffixIcon: Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: TextFieldAddIconButton(
                          onPressed: () {
                            setState(() {});
                          },
                          playerController: playerController,
                          playerInputController: _playerInputController,
                        )),
                    suffixIconColor: Theme.of(context).primaryColor,
                    enabledBorder: outlinedBorder,
                    focusedBorder: outlinedBorder,
                    hintText: 'add_player'.tr,
                    hintStyle:
                        TextStyle(color: Theme.of(context).primaryColor)),
                autocorrect: false),
          ),
          const SizedBox(
            width: 4.0,
          ),
          TextFieldStartButton(
            height: textFieldHeight,
            playerController: playerController,
            startGame: startGame,
          )
        ],
      ),
    );
  }
}

class TextFieldAddIconButton extends StatelessWidget {
  const TextFieldAddIconButton(
      {super.key,
      required this.playerInputController,
      required this.playerController,
      required this.onPressed});
  final TextEditingController playerInputController;
  final PlayerController playerController;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      icon: const Icon(Icons.add),
      tooltip: 'add'.tr,
      onPressed: () {
        if (playerInputController.text.isEmpty) {
        } else {
          if (playerController.canPlayerBeAdded(playerInputController.text)) {
            playerController
                .addPlayer(Player(name: playerInputController.text));
            playerInputController.clear();
            onPressed();
          } else {
            Vibrate.feedback(FeedbackType.error);
          }
        }
      },
    );
  }
}

void unfocuseAllFocus(BuildContext context) {
  FocusScope.of(context).requestFocus(FocusNode());
}
