import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:trinkspiel/UI/Buttons/selection_button.dart';
import 'package:trinkspiel/UI/Buttons/togglable_start_button.dart';
import 'package:trinkspiel/UI/screens/Shop_screen.dart';
import 'package:trinkspiel/UI/screens/settings_screen.dart';
import 'package:trinkspiel/controller/mode_controller.dart';

class StartingScreen extends StatelessWidget {
  const StartingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    ModeController modeController = Get.put(ModeController());

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            tooltip: 'starting_screen_shopbutton_tooltip'.tr,
            onPressed: () => {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ShopScreen()))
                },
            icon: const Icon(Icons.shopping_bag)),
        actions: [
          IconButton(
              tooltip: 'starting_screen_settingsbutton_tooltip'.tr,
              onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const SettingsScreen()))
                  },
              icon: const Icon(Icons.settings))
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              padding: const EdgeInsets.only(top: 20),
              itemCount: modeController.activeModes.length,
              itemBuilder: (context, index) {
                return ListDeckTile(index: index);
              },
            ),
          ),
          TogglableStartButton(
            modeController: modeController,
          )
        ],
      ),
    );
  }
}

class ListDeckTile extends StatelessWidget {
  const ListDeckTile({super.key, required this.index});

  final int index;

  @override
  Widget build(BuildContext context) {
    ModeController modeController = Get.find();

    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SelectionButton(
            text: Text(deckToStringMap[Decks.values[index]]),
            onPressed: (isPressed) {
              modeController.changeActiveMode(index);
            },
            isPressed: modeController.isDeckActive(index)),
      ),
    );
  }
}
