import 'package:flutter/material.dart' hide BoxDecoration, BoxShadow;
import 'package:flutter_inset_box_shadow/flutter_inset_box_shadow.dart';
import 'package:get/get.dart';

import 'package:trinkspiel/UI/ui_constants.dart';
import 'package:trinkspiel/controller/player_controller.dart';

import '../../model/player.dart';
import '../IO/name_input_field.dart';

// not more than 30 players and no special characters - still have to do: at 30 players pop up text that 30 is limit
// Haptic feedback if no player is selected and the start button is darker
// is it enough to give this haptic feedback or is an pop up text needed?

class NameSelectionScreen extends StatelessWidget {
  const NameSelectionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    PlayerController playerController = Get.put(PlayerController());
    return GestureDetector(
      onPanUpdate: (details) =>
          details.delta.dy > 0 ? unfocuseAllFocus(context) : (details) {},
      onTap: () => unfocuseAllFocus(context),
      child: Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            title: Text(
              'player_selection'.tr,
              style: TextStyle(color: Theme.of(context).primaryColor),
            )),
        body: Column(
          children: [Flexible(child: PlayerListWidget()), const PlayerInput()],
        ),
      ),
    );
  }
}

List<Widget> playerListToWidgetList(List<Player> players) {
  List<Widget> widgetList = [];
  for (int i = 0; i < players.length; i++) {
    widgetList.add(NameItemWidget(player: players[i]));
  }
  return widgetList;
}

class PlayerListWidget extends StatelessWidget {
  PlayerListWidget({super.key});
  final PlayerController playerController = Get.find();

  @override
  Widget build(BuildContext context) {
    final Color shadow = Color.lerp(
        Theme.of(context).scaffoldBackgroundColor, Colors.black, 0.4)!;
    const Offset offset = Offset(10, 10);
    const double blurRadius = 15.0;

    return Container(
      foregroundDecoration:
          BoxDecoration(borderRadius: standardBorderRadius, boxShadow: [
        BoxShadow(
            color: shadow,
            offset: -offset,
            blurRadius: blurRadius,
            inset: true),
        BoxShadow(
            color: shadow, offset: offset, blurRadius: blurRadius, inset: true)
      ]),
      height: MediaQuery.of(context).size.height * 0.675,
      width: MediaQuery.of(context).size.width * 0.94,
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 2.0, vertical: 6.0),
          child: Obx(
            () => SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: playerController.players.isNotEmpty
                  ? Wrap(
                      direction: Axis.horizontal,
                      children: playerListToWidgetList(
                          // ignore: invalid_use_of_protected_member
                          playerController.players.value.cast<Player>()),
                    )
                  : Center(
                      heightFactor: MediaQuery.of(context).size.height * 0.0106,
                      child: Text(
                        'player_list'.tr,
                        style: TextStyle(
                            fontSize: MediaQuery.of(context).size.height * 0.04,
                            color: Theme.of(context).primaryColor),
                      ),
                    ),
            ),
          )),
    );
  }
}

class NameItemWidget extends StatelessWidget {
  NameItemWidget({super.key, required this.player});
  final Player player;
  final PlayerController playerController = Get.find();

  @override
  Widget build(BuildContext context) {
    final double displaySize = // more or less the size
        MediaQuery.of(context).size.height + MediaQuery.of(context).size.width >
                3200.0
            ? 3200
            : MediaQuery.of(context).size.height +
                MediaQuery.of(context).size.width;
    return Container(
      decoration: BoxDecoration(
          borderRadius: standardBorderRadius,
          color: Theme.of(context).primaryColor),
      height: displaySize * 0.044053,
      margin: const EdgeInsets.all(8.0),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0.0, 8.0, 12.0, 8.0),
        child: FittedBox(
          child: Row(
            children: [
              IconButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  playerController.deletePlayer(player);
                },
                icon: const Icon(Icons.remove),
                iconSize: displaySize * 0.0139,
                color: Theme.of(context).scaffoldBackgroundColor,
              ),
              Text(
                player.name,
                style: TextStyle(
                    color: Theme.of(context).scaffoldBackgroundColor,
                    fontSize: displaySize * 0.02),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
