import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trinkspiel/UI/screens/starting_screen.dart';
import 'package:trinkspiel/controller/game_controller.dart';

class GameScreen extends StatelessWidget {
  const GameScreen({super.key});

  @override
  Widget build(BuildContext context) {
    GameController gameController = Get.put(GameController());
    return Obx(() => GestureDetector(
          onTap: () {
            gameController.getNextText();
          },
          child: Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              leading: IconButton(
                tooltip: 'game_screen_homebutton_tooltip'.tr,
                onPressed: () => {Get.offAll(() => const StartingScreen())},
                icon: const Icon(Icons.home_outlined),
                iconSize: 35,
              ),
            ),
            body: Center(
                child: Column(children: [
              Text(gameController.currentCardText.value,
                  style: const TextStyle(fontSize: 12))
            ])),
          ),
        ));
  }
}
