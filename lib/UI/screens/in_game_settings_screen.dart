import 'dart:math';

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:trinkspiel/UI/screens/name_selection_screen.dart';
import 'package:trinkspiel/controller/in_game_settings_controller.dart';

import '../Buttons/continue_button.dart';
import '../Buttons/selection_button.dart';
import '../ui_constants.dart';

// Selection Button Size: 0,8 * screen width; 75 high

/*
  desired behaviour:
    - language translation at each text
    - saving last state of in game settings even when app closed (next time open the same settings should be pre selected)
    - only one button is selected at the toggle buttons/ 'setting bars'
*/

class InGameSettingsScreen extends StatelessWidget {
  InGameSettingsScreen({super.key}) {
    // ignore: unused_local_variable
    InGameSettingsController inGameController =
        Get.put(InGameSettingsController());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text(
            'in-Game-Settings'.tr,
            style: TextStyle(color: Theme.of(context).primaryColor),
          )),
      body: Center(
        child: Column(
          children: [
            Flexible(flex: 1, child: Container()),
            const Flexible(
              flex: 2,
              fit: FlexFit.loose,
              child: DifficultyBar(),
            ),
            Flexible(flex: 1, child: Container()),
            const Flexible(fit: FlexFit.loose, flex: 2, child: HotnessBar()),
            Flexible(flex: 2, child: Container()),
            Flexible(
                fit: FlexFit.loose, flex: 6, child: NeededActionsButtons()),
            Flexible(flex: 1, child: Container()),
            const Flexible(
                flex: 2,
                fit: FlexFit.loose,
                child: ContinueButton(
                  nextScreen: NameSelectionScreen(),
                ))
          ],
        ),
      ),
    );
  }
}

class DifficultyBar extends StatefulWidget {
  const DifficultyBar({super.key});

  @override
  State<DifficultyBar> createState() => _DifficultyBarState();
}

class _DifficultyBarState extends State<DifficultyBar> {
  List<bool> _difficultySelections = [true, false, false, false];
  InGameSettingsController inGameController = Get.find();

  _DifficultyBarState() {
    _difficultySelections = [
      (0 == inGameController.getInGameDifficulty().index),
      (1 == inGameController.getInGameDifficulty().index),
      (2 == inGameController.getInGameDifficulty().index),
      (3 == inGameController.getInGameDifficulty().index)
    ];
  }

  @override
  Widget build(BuildContext context) {
    Color primaryColor = Theme.of(context).primaryColor;
    Color secondColor = Theme.of(context).scaffoldBackgroundColor;
    return ToggleButtons(
        color: primaryColor,
        fillColor: primaryColor,
        selectedColor: secondColor,
        borderColor: primaryColor,
        selectedBorderColor: primaryColor,
        borderWidth: buttonBorderWidth,
        constraints: const BoxConstraints.expand(width: 80, height: 80),
        borderRadius: standardBorderRadius,
        onPressed: (index) {
          setState(() {
            for (int i = 0; i < _difficultySelections.length; i++) {
              _difficultySelections[i] = i == index;
            }
            inGameController
                .setInGameDifficulty(InGameDifficulty.values[index]);
          });
        },
        isSelected: _difficultySelections,
        children: [
          ToggleButton(
            icon: Icons.sunny,
            isSelected: _difficultySelections[0],
            text: Text('soft'.tr),
          ),
          ToggleButton(
            icon: Icons.sunny,
            isSelected: _difficultySelections[1],
            text: Text('Medium'.tr),
          ),
          ToggleButton(
            icon: Icons.sunny,
            isSelected: _difficultySelections[2],
            text: Text('Hard'.tr),
          ),
          ToggleButton(
            icon: Icons.sunny,
            isSelected: _difficultySelections[3],
            text: Text('difficulty_extrem'.tr),
          ),
        ]);
  }
}

class HotnessBar extends StatefulWidget {
  const HotnessBar({super.key});

  @override
  State<HotnessBar> createState() => _HotnessBarState();
}

class _HotnessBarState extends State<HotnessBar> {
  List<bool> _hotnessSelections = [true, false];
  InGameSettingsController inGameController = Get.find();

  _HotnessBarState() {
    _hotnessSelections = [
      (0 == inGameController.getInGameHotness().index),
      (1 == inGameController.getInGameHotness().index),
    ];
  }

  @override
  Widget build(BuildContext context) {
    Color primaryColor = Theme.of(context).primaryColor;
    Color secondColor = Theme.of(context).scaffoldBackgroundColor;

    double barWidth =
        min(MediaQuery.of(context).size.width * 0.32, maxWidthOfWidget);

    return ToggleButtons(
        color: primaryColor,
        fillColor: primaryColor,
        selectedColor: secondColor,
        borderColor: primaryColor,
        selectedBorderColor: primaryColor,
        borderWidth: buttonBorderWidth,
        constraints: BoxConstraints.expand(width: barWidth, height: 80),
        borderRadius: standardBorderRadius,
        onPressed: (index) {
          setState(() {
            for (int i = 0; i < _hotnessSelections.length; i++) {
              _hotnessSelections[i] = i == index;
            }
            inGameController.setInGameHotness(InGameHotness.values[index]);
          });
        },
        isSelected: _hotnessSelections,
        children: [
          ToggleButton(
            icon: Icons.sunny,
            isSelected: _hotnessSelections[0],
            text: Text('normal_hotness'.tr),
          ),
          ToggleButton(
            icon: Icons.sunny,
            isSelected: _hotnessSelections[1],
            text: Text('18+'.tr),
          ),
        ]);
  }
}

class ToggleButton extends StatelessWidget {
  const ToggleButton(
      {super.key,
      required this.isSelected,
      required this.icon,
      required this.text});
  final bool isSelected;
  final IconData icon;
  final Text text;

  @override
  Widget build(BuildContext context) {
    return isSelected
        ? FittedBox(
            child: Row(
            children: [
              Icon(icon),
              const SizedBox(
                width: 3.0,
              ),
              text
            ],
          ))
        : Icon(icon);
  }
}

class NeededActionsButtons extends StatelessWidget {
  NeededActionsButtons({super.key});

  final InGameSettingsController _inGameController = Get.find();

  @override
  Widget build(BuildContext context) {
    double inBetweenPadding = MediaQuery.of(context).size.height * 0.02;
    return Column(
      children: [
        SelectionButton(
            text: Text('moving_needed'.tr),
            onPressed: _inGameController.setIsMovingNeededSetting,
            isPressed: _inGameController.getIsMovingNeededSetting()),
        SizedBox(
          height: inBetweenPadding,
        ),
        SelectionButton(
          text: Text('phone_needed'.tr),
          onPressed: _inGameController.setIsPhoneUsedSetting,
          isPressed: _inGameController.getIsPhoneUsedSetting(),
        ),
        SizedBox(
          height: inBetweenPadding,
        ),
        SelectionButton(
            text: Text('ownCardsOn'.tr),
            onPressed: _inGameController.setOwnCardsActive,
            isPressed: _inGameController.getOwnCardsActive())
      ],
    );
  }
}
