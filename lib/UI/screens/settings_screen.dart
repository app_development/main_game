import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trinkspiel/UI/Theme/theme_manager.dart';
import 'package:trinkspiel/UI/ui_changing_var.dart';
import 'package:trinkspiel/controller/shared_preferences_controller.dart';
import 'package:trinkspiel/model/setting.dart';

import '../../widgets/setting_tile.dart';
import '../ui_constants.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("settings".tr),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        inAppSettings.length,
                        (index) => SettingTile(setting: inAppSettings[index]),
                      ),
                    ),
                    const Divider(
                      thickness: 1.5,
                    ),
                    const SizedBox(height: 10),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        furtherSettings.length,
                        (index) => SettingTile(setting: furtherSettings[index]),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Text(
              "Version: ",
              style: TextStyle(fontSize: smallFontSize),
            ),
            const SizedBox(
              height: fixedBottomSizedBoxPadding,
            )
          ],
        ),
      ),
    );
  }
}

void changeTheme() {
  if (1 == SharedPreferencesController.getPrefs().getInt("currTheme")) {
    ThemeManager().setTheme(Themes.standardLightTheme);
  } else {
    ThemeManager().setTheme(Themes.standardDarkTheme);
  }
}


  // ListView.builder(
            //   physics: const NeverScrollableScrollPhysics(),
            //   padding: const EdgeInsets.only(top: 20),
            //   itemCount: inAppSettings.length,
            //   itemBuilder: (context, index) {
            //     return Center(
            //       child: SettingTile(
            //         setting: inAppSettings[index],
            //       ),
            //     );
            //   },
            // ),
            // ListView.builder(
            //   physics: const NeverScrollableScrollPhysics(),
            //   padding: const EdgeInsets.only(top: 20),
            //   itemCount: inAppSettings.length,
            //   itemBuilder: (context, index) {
            //     return Center(
            //       child: SettingTile(
            //         setting: inAppSettings[index],
            //       ),
            //     );
            //   },
            // ),