import 'package:flutter/material.dart';
import 'package:trinkspiel/UI/Theme/theme_manager.dart';

Color beeYellow = const Color.fromARGB(255, 252, 212, 33);
Color orangeYellow = const Color.fromARGB(255, 252, 197, 30);

Color black = const Color.fromARGB(240, 31, 31, 31);
Color white = const Color.fromARGB(240, 240, 240, 240);
Color disabled = const Color.fromARGB(200, 51, 51, 51);

ThemeData beeYellowTheme = ThemeData.light().copyWith(
  extensions: <ThemeExtension<ExtraColors>>[extraColors],
  scaffoldBackgroundColor: beeYellow,
  primaryColor: black,
  disabledColor: disabled,
  visualDensity: VisualDensity.adaptivePlatformDensity,
  appBarTheme:
      const AppBarTheme(backgroundColor: Colors.transparent, elevation: 0.0),
);

ExtraColors extraColors = ExtraColors(darkOutBlurredColor: Colors.black12);
