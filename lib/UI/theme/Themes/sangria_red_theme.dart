import 'package:flutter/material.dart';

Color standardLightThemeBackGroundColor =
    const Color.fromARGB(202, 128, 14, 19);
Color standardLightThemeWhite = const Color.fromARGB(202, 253, 240, 213);
Color disabled = const Color.fromARGB(202, 128, 14, 19);
// beige: Color.fromARGB(202, 253, 240, 213);

ThemeData sangriaRedTheme = ThemeData.light().copyWith(
    scaffoldBackgroundColor: standardLightThemeBackGroundColor,
    primaryColor: standardLightThemeWhite,
    disabledColor: disabled,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    appBarTheme:
        const AppBarTheme(backgroundColor: Colors.transparent, elevation: 0.0));
