import 'package:flutter/material.dart';

Color standardDarkThemeBlack = const Color.fromARGB(0, 0, 0, 0);
Color standardDarkThemeWhite = const Color.fromARGB(250, 252, 252, 252);

ThemeData darkTheme = ThemeData.light().copyWith(
  // scaffoldBackgroundColor: standardDarkThemeBlack,
  // primaryColor: standardDarkThemeWhite,
  visualDensity: VisualDensity.adaptivePlatformDensity,
  // appBarTheme:
  //     AppBarTheme(backgroundColor: Colors.transparent, elevation: 0.0));
);
