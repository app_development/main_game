// Get.changeThemeMode(ThemeData)
// ThemeData(extension: ) own Variables
// ThemeData().light.copyWith(changes)
//Color.fromARGB(245, 20, 245, 245)
//Color.fromARGB(255, 20, 245, 245);
// hexCode: #14F5F5
// H: 180, S: 92, V:96; RGB: 20, 245, 245

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trinkspiel/UI/Theme/Themes/dark_theme.dart';
import 'package:trinkspiel/UI/Theme/Themes/sangria_red_theme.dart';
import 'package:trinkspiel/controller/shared_preferences_controller.dart';
import '../Theme/Themes/bee_yellow_theme.dart';

// this enum has to have the same order as the themes in _themes (list)
enum Themes {
  standardLightTheme,
  standardDarkTheme,
  beeYellowTheme,
  sangriaRedTheme,
  darkTheme
}

ThemeData standardLightTheme = beeYellowTheme;
ThemeData standardDarkTheme = darkTheme;

class ThemeManager extends GetxController {
  final List<ThemeData> _themes = [
    standardLightTheme,
    standardDarkTheme,
    beeYellowTheme,
    sangriaRedTheme,
    darkTheme
  ];

  ThemeData getTheme(Themes theme) => _themes[theme.index];
  ThemeData getThemeFromIndex(int index) => _themes[index];

  void setTheme(Themes theme) {
    setThemeFromIndex(theme.index);
  }

  void setThemeFromIndex(int index) {
    SharedPreferencesController.getPrefs().setInt("currTheme", index);
    Get.changeTheme(_themes[index]);
  }
}

class ExtraColors extends ThemeExtension<ExtraColors> {
  Color darkOutBlurredColor;

  ExtraColors({required this.darkOutBlurredColor});

  @override
  ExtraColors copyWith({Color? darkOutBlurredColor}) => ExtraColors(
      darkOutBlurredColor: darkOutBlurredColor ?? this.darkOutBlurredColor);

  @override
  ExtraColors lerp(ThemeExtension<ExtraColors> other, double t) {
    if (other is! ExtraColors) return this;

    return ExtraColors(
        darkOutBlurredColor:
            Color.lerp(darkOutBlurredColor, other.darkOutBlurredColor, t)!);
  }
}
