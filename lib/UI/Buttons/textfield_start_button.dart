import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_vibrate/flutter_vibrate.dart';

import '../../controller/player_controller.dart';
import '../ui_constants.dart';

class TextFieldStartButton extends StatelessWidget {
  // maybe: OverlayEntry
  const TextFieldStartButton(
      {super.key,
      required this.playerController,
      required this.startGame,
      required this.height});
  final PlayerController playerController;
  final Function(BuildContext) startGame;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Obx(() => ElevatedButton(
        // ignore: invalid_use_of_protected_member
        onPressed: playerController.players.value.isEmpty
            ? () {
                Vibrate.feedback(FeedbackType.error);
              } // haptic Feedback and a banner not being able to start without players
            : () {
                startGame(context);
              },
        style: ElevatedButton.styleFrom(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.horizontal(
                    right: Radius.circular(buttonBorderRadiusSize))),
            // ignore: invalid_use_of_protected_member
            backgroundColor: playerController.players.value.isEmpty
                ? Theme.of(context).disabledColor
                : Theme.of(context).primaryColor,
            foregroundColor: Theme.of(context).scaffoldBackgroundColor,
            fixedSize: Size(70.0, height)),
        child: FittedBox(
          child: Text(
            'start'.tr,
            style: TextStyle(fontSize: height),
          ),
        ))); // 70.0 , 59.0 iPhone 14 Pro Max;
  }
}
