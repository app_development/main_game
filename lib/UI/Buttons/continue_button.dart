import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../ui_constants.dart';

class ContinueButton extends StatelessWidget {
  const ContinueButton({super.key, required this.nextScreen});
  final Widget nextScreen;

  @override
  Widget build(BuildContext context) {
    final double currWidth = MediaQuery.of(context).size.width * 0.55;
    const double maxWidth = 467.5;
    return ElevatedButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => nextScreen));
        },
        style: ElevatedButton.styleFrom(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                    Radius.circular(buttonBorderRadiusSize * 2.5))),
            backgroundColor: Theme.of(context).primaryColor,
            foregroundColor: Theme.of(context).scaffoldBackgroundColor,
            fixedSize: Size(currWidth > maxWidth ? maxWidth : currWidth,
                MediaQuery.of(context).size.height * 0.070472103)),
        child: Text(
          'continue'.tr,
          style: const TextStyle(fontSize: 20),
        ));
  }
}
