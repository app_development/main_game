import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:get/get.dart';
import 'package:flutter_vibrate/flutter_vibrate.dart';

import 'package:trinkspiel/UI/screens/in_game_settings_screen.dart';
import 'package:trinkspiel/controller/mode_controller.dart';

import '../ui_constants.dart';

class TogglableStartButton extends StatelessWidget {
  const TogglableStartButton({super.key, required this.modeController});
  final ModeController modeController;

  @override
  Widget build(BuildContext context) {
    double buttonWidth =
        min(MediaQuery.of(context).size.width * 0.85, maxWidthOfWidget * 0.8);
    double buttonHeight = min(standardButtonmaxHeight,
        MediaQuery.of(context).size.width * 0.2093023256);

    double startTextSize = buttonWidth * 0.109;
    double animationMovingWidth = 8.0;
    Duration animationDelay = const Duration(milliseconds: 100);
    Duration animationDuration = const Duration(milliseconds: 500);

    double iconSize = buttonWidth * 0.22;

    return Obx(() {
      bool isOneDeckSelected = modeController.activeDecksCount.value == 0;
      return Padding(
          padding: const EdgeInsets.symmetric(vertical: 60),
          child: ElevatedButton(
              onPressed: isOneDeckSelected
                  ? () {
                      Vibrate.feedback(FeedbackType.error);
                    }
                  : () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => InGameSettingsScreen()));
                    },
              style: ElevatedButton.styleFrom(
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                          Radius.circular(buttonBorderRadiusSize * 2.5))),
                  backgroundColor: isOneDeckSelected
                      ? Theme.of(context).disabledColor
                      : Theme.of(context).primaryColor,
                  foregroundColor: Theme.of(context).scaffoldBackgroundColor,
                  fixedSize: Size(buttonWidth, buttonHeight)),
              child: Center(
                child: Row(
                  children: [
                    Flexible(
                      flex: 5,
                      child: Column(
                        children: [
                          Flexible(flex: 1, child: Container()),
                          Flexible(
                            flex: 4,
                            child: Text(
                              'continue'.tr,
                              style: TextStyle(fontSize: startTextSize),
                            ),
                          ),
                          Flexible(
                            flex: 2,
                            child: Text(
                                "Decks: ${modeController.activeDecksCount.value}, Cards: ${modeController.getNumberOfActiveCards()}",
                                style: TextStyle(
                                    fontSize: startTextSize * 0.40295017)),
                          ),
                        ],
                      ),
                    ),
                    Flexible(
                        flex: 2,
                        child: isOneDeckSelected
                            ? Icon(
                                Icons.play_arrow_outlined,
                                size: iconSize,
                              )
                            : Icon(
                                Icons.play_arrow_outlined,
                                size: iconSize,
                              )
                                .animate(
                                  onPlay: (controller) =>
                                      controller.repeat(reverse: true),
                                )
                                .scale(
                                    delay: animationDelay,
                                    end: const Offset(0.85, 0.85),
                                    duration: animationDuration)
                                .moveX(
                                    delay: animationDelay,
                                    begin: 0,
                                    end: animationMovingWidth,
                                    duration: animationDuration))
                  ],
                ),
              )));
    });
  }
}
