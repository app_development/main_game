import 'package:flutter/material.dart';

import '../ui_constants.dart';

class SelectionButton extends StatefulWidget {
  const SelectionButton(
      {super.key,
      required this.text,
      required this.onPressed,
      required this.isPressed});

  final Text text;
  final void Function(bool) onPressed;
  final bool isPressed;

  @override
  // ignore: no_logic_in_create_state
  State<SelectionButton> createState() => _SelectionButtonState(
      text: text, onPressed: onPressed, isPressed: isPressed);
}

class _SelectionButtonState extends State<SelectionButton> {
  _SelectionButtonState(
      {required this.text, required this.onPressed, required this.isPressed});

  final Text text;
  final void Function(bool) onPressed;
  bool isPressed;

  @override
  Widget build(BuildContext context) {
    // ignore: no_leading_underscores_for_local_identifiers
    _onPressed() {
      setState(() {
        isPressed = !isPressed;
        onPressed(isPressed);
      });
    }

    final double currWidth = MediaQuery.of(context).size.width * 0.8;
    const double maxWidth = 680;
    final Size fixedSize = Size(currWidth > maxWidth ? maxWidth : currWidth,
        MediaQuery.of(context).size.height * 0.080472103);

    const RoundedRectangleBorder border =
        RoundedRectangleBorder(borderRadius: standardBorderRadius);

    return Wrap(spacing: 100.0, children: [
      isPressed
          ? ElevatedButton(
              style: ElevatedButton.styleFrom(
                  shape: border,
                  fixedSize: fixedSize,
                  backgroundColor: Theme.of(context).primaryColor,
                  foregroundColor: Theme.of(context).scaffoldBackgroundColor),
              onPressed: _onPressed,
              child: text)
          : OutlinedButton(
              style: OutlinedButton.styleFrom(
                  shape: border,
                  fixedSize: fixedSize,
                  side: BorderSide(
                      width: buttonBorderWidth,
                      color: Theme.of(context).primaryColor),
                  foregroundColor: Theme.of(context).primaryColor),
              onPressed: _onPressed,
              child: text)
    ]);
  }
}
