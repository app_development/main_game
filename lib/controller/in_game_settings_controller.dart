import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:trinkspiel/controller/shared_preferences_controller.dart';

enum InGameDifficulty { soft, medium, hard, extrem }

enum InGameHotness { normal, extrem }

// print(InGameDifficulty.soft.index);
// print(index);
// print(InGameDifficulty.values[index]);

class InGameSettingsController extends GetxController {
  InGameDifficulty _inGameDifficulty = InGameDifficulty.soft;
  InGameHotness _inGameHotness = InGameHotness.normal;
  bool _isPhoneUsedSetting = false;
  bool _isMovingNeededSetting = false;
  bool _ownCardsActive = false;

  InGameSettingsController() {
    if (!SharedPreferencesController.isSharedPrefsInitialized()) {
      return;
    }
    if (SharedPreferencesController.getPrefs()
        .containsKey("in_game_difficulty")) {
      _inGameDifficulty = InGameDifficulty.values[
          SharedPreferencesController.getPrefs().getInt("in_game_difficulty")!];
    }
    if (SharedPreferencesController.getPrefs().containsKey("in_game_hotness")) {
      _inGameHotness = InGameHotness.values[
          SharedPreferencesController.getPrefs().getInt("in_game_hotness")!];
    }
    if (SharedPreferencesController.getPrefs()
        .containsKey("is_phone_used_setting")) {
      _isPhoneUsedSetting = SharedPreferencesController.getPrefs()
          .getBool("is_phone_used_setting")!;
    }
    if (SharedPreferencesController.getPrefs()
        .containsKey("is_moving_needed_setting")) {
      _isMovingNeededSetting = SharedPreferencesController.getPrefs()
          .getBool("is_moving_needed_setting")!;
    }
    if (SharedPreferencesController.getPrefs()
        .containsKey("own_cards_active_setting")) {
      _ownCardsActive = SharedPreferencesController.getPrefs()
          .getBool("own_cards_active_setting")!;
    }
  }

  InGameDifficulty getInGameDifficulty() => _inGameDifficulty;

  void setInGameDifficulty(InGameDifficulty inGameDifficulty) {
    if (SharedPreferencesController.isSharedPrefsInitialized()) {
      SharedPreferencesController.getPrefs()
          .setInt("in_game_difficulty", inGameDifficulty.index);
    }

    _inGameDifficulty = inGameDifficulty;
  }

  InGameHotness getInGameHotness() => _inGameHotness;

  void setInGameHotness(InGameHotness inGameHotness) {
    SharedPreferencesController.getPrefs()
        .setInt("in_game_hotness", inGameHotness.index);
    _inGameHotness = inGameHotness;
  }

  bool getIsPhoneUsedSetting() => _isPhoneUsedSetting;

  void setIsPhoneUsedSetting(bool isPhoneUsed) {
    SharedPreferencesController.getPrefs()
        .setBool("is_phone_used_setting", isPhoneUsed);
    _isPhoneUsedSetting = isPhoneUsed;
  }

  bool getIsMovingNeededSetting() => _isMovingNeededSetting;

  void setIsMovingNeededSetting(bool isMovingNeededSetting) {
    SharedPreferencesController.getPrefs()
        .setBool("is_moving_needed_setting", isMovingNeededSetting);
    _isMovingNeededSetting = isMovingNeededSetting;
  }

  bool getOwnCardsActive() => _ownCardsActive;

  void setOwnCardsActive(bool ownCardsActive) {
    SharedPreferencesController.getPrefs()
        .setBool("own_cards_active_setting", ownCardsActive);
    _ownCardsActive = ownCardsActive;
  }
}
