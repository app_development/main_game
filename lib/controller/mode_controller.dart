import 'package:get/get.dart';
import 'package:sqflite/sqflite.dart';

import 'db_controller.dart';

enum Decks { truth, dare, neverHaveIEver, whoWouldRather, standard }

Map<Decks, List<int>> deckContainsSubmodi = {
  Decks.truth: [1],
  Decks.dare: [2],
  Decks.neverHaveIEver: [3],
  Decks.whoWouldRather: [4],
  Decks.standard: [5, 6, 7, 8]
};

Map deckToStringMap = <Decks, String>{
  Decks.truth: 'truth_mode',
  Decks.dare: 'dare_mode',
  Decks.neverHaveIEver: 'never_have_i_ever_mode',
  Decks.whoWouldRather: 'who_would_rather_mode',
  Decks.standard: 'standard_mode'
};

class ModeController extends GetxController {
  RxList<bool> activeModes = [false, false, false, false, false].obs;
  RxInt activeDecksCount = 0.obs;
  RxInt numberOfActiveCards = 0.obs;

  int getNumberOfActiveCards() => numberOfActiveCards.value;

  Future<void> setNumberOfActiveCards() async => numberOfActiveCards
      .value = Sqflite.firstIntValue(await DBController
          .getDB()
      .then((value) => value.rawQuery(
          'SELECT Count(*) FROM gamecards WHERE ${getActiveModeSQLCondition()};')))!;

  void changeActiveMode(int index) {
    // ignore: invalid_use_of_protected_member
    activeModes.value[index] = !activeModes.value[index];
    // ignore: invalid_use_of_protected_member
    activeModes.value[index] ? activeDecksCount++ : activeDecksCount--;

    setNumberOfActiveCards();
  }

  void changeActiveModeFromDeck(Decks deck) {
    // ignore: invalid_use_of_protected_member
    changeActiveMode(deck.index);
  }

  String getActiveModeSQLCondition() {
    String where = "modus IN (";
    deckContainsSubmodi.forEach((key, value) {
      if (isDeckActiveFromDeck(key)) {
        for (var e in value) {
          where += "$e,";
        }
      }
    });

    if (where[where.length - 1] == ",") {
      where = where.substring(0, where.length - 1);
    }
    where += ")";
    return where;
  }

  bool isDeckActiveFromDeck(Decks deck) {
    return isDeckActive(deck.index);
  }

  bool isDeckActive(int index) {
    // ignore: invalid_use_of_protected_member
    return activeModes.value[index];
  }
}
