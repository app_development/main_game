import 'dart:math';

import 'package:get/get.dart';
import 'package:trinkspiel/Exceptions/controller_exceptions/card_pre_processing_exceptions.dart';
import 'package:trinkspiel/controller/in_game_settings_controller.dart';

import 'package:trinkspiel/controller/player_controller.dart';
import 'package:trinkspiel/model/game_card.dart';
import 'package:tuple/tuple.dart';

import '../model/player.dart';

List<String> cardPreProcessing(String cardText, CardType cardType) {
  // refactor with split at $
  PlayerController playerController = Get.find();
  List<Player?> playersAlreadyIDgiven =
      List.filled(playerController.players.length, null);

  List<String> resultList = List.empty(growable: true);
  String partResult = "";

  partResult += createModeSpecificStandardTex(cardType);

  int currentIndex = 0;
  while (currentIndex < cardText.length) {
    if (cardText[currentIndex] != "\$") {
      partResult += cardText[currentIndex];
      currentIndex++;
      continue;
    } else if (cardText[currentIndex] == "\$") {
      int specialOperatorIndex =
          1; // specialOperatorIndex + 1 is the length of $...$
      while (cardText[currentIndex + specialOperatorIndex] != "\$") {
        specialOperatorIndex++;
      }

      int specialOperatorLength = specialOperatorIndex + 1;

      if (cardText[currentIndex + 1] == "-") {
        // removing the blankspaces after the text before the next $-$
        while (partResult[partResult.length - 1] == " ") {
          partResult = partResult.substring(0, partResult.length - 1);
        }

        // adding
        resultList.add(partResult);
        partResult = "";

        // removing the blankspaces in front of the new text after the $-$
        int blankSpaceCounter = 0;
        while (cardText[
                currentIndex + specialOperatorIndex + 1 + blankSpaceCounter] ==
            " ") {
          blankSpaceCounter++;
        }
        currentIndex += blankSpaceCounter;
      } else {
        //asd $-$  This
        Tuple2 specialOperatorResolvedTuple = specialOperation(
            cardText.substring(
                currentIndex, currentIndex + specialOperatorLength),
            playersAlreadyIDgiven,
            playerController);
        partResult += specialOperatorResolvedTuple.item1;
        playersAlreadyIDgiven = specialOperatorResolvedTuple.item2;
      }
      currentIndex += specialOperatorLength;
    }
  }
  resultList.add(partResult);

  return resultList;
}

/*
  processes different specialOperators
  returns the value which should replace the specialoperator $...$
*/
Tuple2<String, List<Player?>> specialOperation(String substring,
    List<Player?> playersAlreadyIDgiven, PlayerController playerController) {
  int index = 1;
  switch (substring[index]) {
    case "-":
      break;

    case "/":
      List<String> options =
          substring.substring(2, substring.length - 1).split('/');
      Random rand = Random();
      substring = options[rand.nextInt(options.length)];
      break;

    case "p":
      int personNumber =
          int.parse(substring.substring(2, substring.length - 1));

      if (personNumber > playersAlreadyIDgiven.length) {
        throw PlayerNumberHigherThanPlayersInGameException();
      }

      int personIndex = personNumber - 1;

      if (playersAlreadyIDgiven[personIndex] == null) {
        Random rand = Random();
        Player currPlayer = playerController
            .players[rand.nextInt(playerController.players.length)];
        playersAlreadyIDgiven[personIndex] = currPlayer;
      }

      substring = playersAlreadyIDgiven[personIndex]!.name;
      break;
    case "s":
      int standardSip = 0;
      try {
        standardSip = int.parse(substring.substring(2, substring.length - 1));
      } catch (e) {
        throw EmptyBehindSpecialOperatorS();
      }
      if (substring.length == 3) {
        throw EmptyBehindSpecialOperatorS();
      }
      InGameSettingsController inGameSettingsController = Get.find();

      switch (inGameSettingsController.getInGameDifficulty()) {
        case InGameDifficulty.soft:
          standardSip *= 1;
          break;
        case InGameDifficulty.medium:
          standardSip *= 2;
          break;
        case InGameDifficulty.hard:
          standardSip *= 3;
          break;
        case InGameDifficulty.extrem:
          standardSip *= 5;
          break;

        default:
          break;
      }

      substring = standardSip.toString();
      break;
    default:
      break;
  }
  return Tuple2(substring, playersAlreadyIDgiven);
}

String createModeSpecificStandardTex(CardType cardType) {
  switch (cardType) {
    case CardType.truth:
      break;
    case CardType.dare:
      break;
    case CardType.standard:
      break;
    case CardType.never_have_i_ever:
      return '${'never_have_i_ever'.tr} '; // there is already a space (look closely)
    case CardType.who_would_rather:
      return '${'who_would_rather'.tr} ';
    default:
  }
  return "";
}
