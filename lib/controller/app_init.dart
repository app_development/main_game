import 'package:shared_preferences/shared_preferences.dart';
import 'package:trinkspiel/controller/shared_preferences_controller.dart';

Future<bool> appInit(bool eraseEverythingFromSP) async {
  SharedPreferencesController.setPrefs(await SharedPreferences.getInstance());

  if (eraseEverythingFromSP) {
    SharedPreferencesController.getPrefs().clear();
  }

  if (!SharedPreferencesController.getPrefs().containsKey('isNewLaunch')) {
    firstLaunchInit();
    SharedPreferencesController.getPrefs().setBool('isNewLaunch', false);
  }

  return true;
}

bool firstLaunchInit() {
  SharedPreferencesController.getPrefs().setInt("currTheme", 0);
  if (!SharedPreferencesController.getPrefs().containsKey('appVersion')) {
    SharedPreferencesController.getPrefs().setInt("appVersion", 0);
  }
  return true;
}
