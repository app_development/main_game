import 'dart:io';
import 'package:flutter/services.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'controller_constants.dart';

class DBController {
  static Future<Database>? _database;

  static Future<Database> getDB() async {
    _database ??= openDatabase(dbFileName, version: 1,
        onCreate: (Database db, int version) async {
      if (version == 1) {
        DBController.copyDatabaseFromAssets(
            await getDatabasesPath(), dbFileName);
      }
    });
    return _database!;
  }

  static Future<bool> copyDatabaseFromAssets(
      String systemDatabasesPath, String dbFileName) async {
    ByteData dataDB = await rootBundle.load("assets/databases/cards_db.db");
    List<int> bytesDB =
        dataDB.buffer.asInt8List(dataDB.offsetInBytes, dataDB.lengthInBytes);
    await File(join(await getDatabasesPath(), "cards_db.db"))
        .writeAsBytes(bytesDB, flush: true);

    return true;
  }
}
