import 'dart:convert';
import 'package:get/get.dart';
import 'package:trinkspiel/controller/shared_preferences_controller.dart';
import 'package:trinkspiel/model/player.dart';

const maxPlayerCount = 30;

class PlayerController extends GetxController {
  RxList players = [].obs;

  bool savePlayersToStorage() {
    if (players.isEmpty) {
      return false;
    }
    // ignore: invalid_use_of_protected_member
    List playerList = players.value.map((e) => e.toJson()).toList();
    SharedPreferencesController.getPrefs()
        .setString('player_list', jsonEncode(playerList));

    return true;
  }

  void addPlayer(Player player) {
    players.add(player);
  }

  List<Player>? getPlayersFromStorage() {
    if (!SharedPreferencesController.isSharedPrefsInitialized() ||
        !SharedPreferencesController.getPrefs().containsKey('player_list')) {
      return null;
    }
    return jsonDecode(
        SharedPreferencesController.getPrefs().getString('player_list')!);
  }

  void deletePlayer(Player player) {
    players.remove(player);
  }

  bool canPlayerBeAdded(String name) {
    // ignore: invalid_use_of_protected_member
    if (players.value.length == maxPlayerCount) return false;

    return true;
  }
}
