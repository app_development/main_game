import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:trinkspiel/UI/screens/game_screen.dart';
import 'package:trinkspiel/controller/card_text_pre_processing.dart';
import 'package:trinkspiel/controller/controller_constants.dart';
import 'package:trinkspiel/controller/in_game_settings_controller.dart';
import 'package:trinkspiel/controller/mode_controller.dart';
import 'package:trinkspiel/controller/shared_preferences_controller.dart';
import 'package:trinkspiel/l10n/localization_controller.dart';
import 'package:trinkspiel/model/game_card.dart';

import 'db_controller.dart';

void startGame(BuildContext context) {
  Get.offAll(() => const GameScreen());
}

class GameController extends GetxController {
  late Future currentFutureCard;
  late String language;
  RxString currentCardText = "".obs;
  late List<String> textQueue = List.empty(growable: true);
  List<String> lastCards = List.empty(growable: true);
  ModeController modeController = Get.find();
  InGameSettingsController inGameSettingsController = Get.find();
  late int
      lastCardsListSize; // how many "old" cards get checked with a new card

  GameController() {
    // local
    language = getLanguageForDBQueries();

    lastCardsListSize =
        min(maxLastCardsListSize, modeController.getNumberOfActiveCards() - 1);

    lastCards = getLastCardsFromStorage() ?? List.empty(growable: true);
    getNextText();
  }

  // text handling -------

  Future getNextText() async {
    while (textQueue.length <= minTextQueueSize) {
      await fillTextQueueWithOneItem();
    }
    String textResult = textQueue.first;
    textQueue.removeAt(0);
    currentCardText.value = textResult;
    return textResult;
  }

  Future<void> fillTextQueueWithOneItem() async {
    bool success = false;
    while (!success) {
      try {
        var card = getCurrentFutureCard();
        Future<String> cardText = card.then((value) =>
            value.first["german"]); // To Do change german to language
        Future<CardType> cardType = card.then((value) => CardType
            .values[value.first["modus"]]); // To Do change modus to category
        List<String> preProcessed = cardPreProcessing(
            await cardText.then((value) => value), await cardType);
        textQueue.addAll(preProcessed);
        success = true;
        // ignore: empty_catches
      } catch (e) {}
    }
  }

  // Future handling ------

  // very unneeded to have nextFutureCard for multiple reasons
  Future getCurrentFutureCard() async {
    currentFutureCard = Future(() async {
      return (await getNewFutureCardFromDB());
    });

    await addCardToLastCards(
        currentFutureCard); // no await here if higher performance needed -> but cards may can occur multiple times
    saveLastCardsToStorage();
    return currentFutureCard;
  }

  // Database interactions ------

  Future getNewFutureCardFromDB() {
    String condition = "";
    condition += modeController.getActiveModeSQLCondition();
    condition += " AND ";
    condition += getNotInLastCardsCondition();
    condition += " AND ";
    condition += getInGameSettingsCondition();

    return DBController.getDB().then((value) => value.rawQuery(
        'SELECT * FROM gamecards WHERE $condition ORDER BY RANDOM() LIMIT 1;'));
  }

  String getNotInLastCardsCondition() {
    if (lastCards.isEmpty) return "1";

    String condition = "id NOT IN (";
    for (int i = 0; i < lastCards.length; i++) {
      condition += "\"${lastCards[i]}\",";
    }
    condition = condition.substring(0, condition.length - 1);
    condition += ")";

    return condition;
  }

  String getInGameSettingsCondition() {
    String condition = "1 ";
    if (inGameSettingsController.getInGameHotness() == InGameHotness.normal) {
      condition += "AND \"18+\" IS False";
    }
    if (!(inGameSettingsController.getIsPhoneUsedSetting())) {
      condition += " AND \"phone\" IS False";
    }
    if (!(inGameSettingsController.getIsMovingNeededSetting())) {
      condition += " AND \"move\" IS False";
    }
    return condition;
  }

  // lastCards ------

  Future<void> addCardToLastCards(Future cardFuture) async {
    if (lastCards.length >= lastCardsListSize) {
      lastCards.removeAt(0);
    }
    lastCards.add((await cardFuture).first["id"].toString());
  }

  bool saveLastCardsToStorage() {
    if (lastCards.isEmpty) {
      return false;
    }
    SharedPreferencesController.getPrefs()
        .setString('lastCards_list', jsonEncode(lastCards));
    return true;
  }

  List<String>? getLastCardsFromStorage() {
    if (!SharedPreferencesController.getPrefs().containsKey('lastCards_list')) {
      return null;
    }
    var jsonDecodedList = jsonDecode(
        SharedPreferencesController.getPrefs().getString('lastCards_list')!);
    List<String> list =
        List<String>.from(jsonDecodedList.map((i) => i)).toList();
    return decreaseListSize(list, lastCardsListSize) as List<String>?;
  }

  /* 
    decreases list until its length == size 
    decreases from the front to the back
  */
  List decreaseListSize(List list, int size) {
    while (list.length >= size) {
      list.removeAt(0);
    }
    return list;
  }
}
