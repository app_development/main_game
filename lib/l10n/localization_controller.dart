import 'dart:ui';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../controller/shared_preferences_controller.dart';

class LocalizationController {
  static Locale? getStartingLocale() {
    String localeLanguageCode;
    SharedPreferences sharedPreferences =
        SharedPreferencesController.getPrefs();
    if (sharedPreferences.containsKey("locale_language_code")) {
      localeLanguageCode =
          sharedPreferences.getString("locale_language_code") as String;
    } else {
      sharedPreferences.setString(
          "locale_language_code", Get.deviceLocale!.languageCode);
      localeLanguageCode = localeLanguageCode =
          sharedPreferences.getString("locale_language_code") as String;
    }
    return Locale(localeLanguageCode);
  }

  static void updateLocalization(String languageCode) {
    SharedPreferencesController.getPrefs()
        .setString("locale_language_code", languageCode);
    Get.updateLocale(Locale(languageCode));
  }
}

String getLanguageForDBQueries() {
  String language = "english";
  try {
    switch (Get.locale!.countryCode) {
      case 'de':
        language = "german";
        break;
      case 'en':
        language = "english";
        break;
      default:
        throw Exception("Language not supported");
    }
    // ignore: empty_catches
  } catch (e) {}
  return language;
}
