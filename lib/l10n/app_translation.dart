import 'package:get/get.dart';

class AppTranslation extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_EN': {
          "language": "english",
          "app_name": "Trinkspiel",
          "starting_screen_tap_to_continue": "(Tap to continue)",
          "starting_screen_settingsbutton_tooltip": "Settings",
          "starting_screen_shopbutton_tooltip": "Shop",
          "game_mode_selection": "Select a Gamemode",
          //--
          "in-Game-Settings": "In Game Settings",
          "continue": "Continue",
          "soft": "Soft",
          "Medium": "Medium",
          "Hard": "Hard",
          "difficulty_extrem": "Extrem",
          "normal_hotness": "Normal",
          "hot_hotness": "18+",
          "moving_needed": "Moving",
          'phone_needed': "Phone",
          'ownCardsOn': "Own Cards",
          //--
          // Name selection screen
          'start': 'Start',
          'add_player': 'Add Player',
          'add': 'Add',
          'player_list': 'Players',
          'player_selection': 'Player Selection',

          //--
          // Game Screen
          "game_screen_homebutton_tooltip": "Home",

          //--
          // Card Pre Processing
          "never_have_i_ever": "Never have I ever",
          "who_would_rather": "Who would rather",
        },
        'de_DE': {
          "language": "Deutsch",
          "app_name": "Trinkspiel",
          "starting_screen_tap_to_continue": "(Drücke um fortzufahren)",
          "starting_screen_settingsbutton_tooltip": "Einstellungen",
          "starting_screen_shopbutton_tooltip": "Shop",
          "game_mode_selection": "Spielmodi Auswahl",
          //--
          "in-Game-Settings": "In Game Einstellungen",
          "continue": "Weiter",
          "soft": "Soft",
          "Medium": "Medium",
          "Hard": "Hard",
          "difficulty_extrem": "Vollsuff",
          "normal_hotness": "Normal",
          "hot_hotness": "18+",
          "moving_needed": "Bewegen",
          'phone_needed': "Handy nötig",
          'ownCardsOn': "Eigene Karten",
          //--
          // Name selection screen
          'start': 'Start',
          'add_player': "Spieler Hinzufügen",
          'add': 'Hinzufügen',
          'player_list': "Spielerliste",
          'player_selection': 'Spielerauswahl',

          //--
          // Game Screen
          "game_screen_homebutton_tooltip": "Startbildschirm",

          //--
          // Card Pre Processing
          "never_have_i_ever": "Ich hab noch nie",
          "who_would_rather": "Wer würde eher",
        }
      };
}
