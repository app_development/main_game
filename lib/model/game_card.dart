class GameCard {
  int? id;
  String germanText;
  String englishText;
  int penalty;
  bool cardInfoAvailable;
  String cardInfo;
  bool isMovingNeeded;
  bool isPhoneUsed;
  bool isInteractingNeeded;
  bool isliked;
  CardType cardType;

  GameCard(
      this.germanText,
      this.englishText,
      this.penalty,
      this.cardInfoAvailable,
      this.cardInfo,
      this.isMovingNeeded,
      this.isPhoneUsed,
      this.isInteractingNeeded,
      this.isliked,
      this.cardType);
}

enum CardType {
  standard,
  counting,
  truth,
  dare,
  couples,
  team,
  never_have_i_ever,
  who_would_rather,
  own_and_liked_cards,
  isTrapCard
}
