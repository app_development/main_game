import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Setting {
  final String title;
  final String route;
  final IconData icon;

  Setting({
    required this.title,
    required this.route,
    required this.icon,
  });
}

final List<Setting> inAppSettings = [
  Setting(
    title: "language",
    route: "/",
    icon: CupertinoIcons.globe,
  ),
  Setting(
      title: "theme",
      route: "/",
      icon: CupertinoIcons
          .rectangle_fill_on_rectangle_fill), // or CupertinoIcons.app
];

final List<Setting> furtherSettings = [
  Setting(
      title: "community_and_FAQ",
      route: "/",
      icon: CupertinoIcons.quote_bubble),
  Setting(title: "rate", route: "/", icon: CupertinoIcons.heart_fill),
  Setting(
      title: "bug_report", route: "/", icon: CupertinoIcons.ant_circle_fill),
  Setting(title: "imprint", route: "/", icon: CupertinoIcons.doc_fill)
];
