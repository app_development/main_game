class Player {
  String name;

  Player({required this.name});

  toJson() {
    return {"name": name};
  }

  fromJson(jsonData) {
    return Player(name: jsonData['name']);
  }
}
