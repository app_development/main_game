import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trinkspiel/UI/Theme/Themes/bee_yellow_theme.dart';

import '../UI/Theme/theme_manager.dart';
import '../model/setting.dart';

class SettingTile extends StatelessWidget {
  final Setting setting;
  const SettingTile({
    super.key,
    required this.setting,
  });

  @override
  Widget build(BuildContext context) {
    double size = 10;
    return GestureDetector(
      onTap: () {}, // Setting
      child: Container(
        color: Colors.transparent,
        child: Row(
          children: [
            Container(
              height: size * 6,
              width: size * 6,
              margin: EdgeInsets.only(bottom: size * 1.5),
              decoration: BoxDecoration(
                color: Theme.of(context)
                    .extension<ExtraColors>()!
                    .darkOutBlurredColor,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Icon(
                setting.icon,
                color: Theme.of(context).primaryColor,
                size: size * 3,
              ),
            ),
            SizedBox(width: size * 1.1),
            Text(
              setting.title,
              style: const TextStyle(
                color: Color.fromARGB(236, 255, 255, 255),
                fontWeight: FontWeight.bold,
              ),
            ),
            const Spacer(),
            Icon(
              CupertinoIcons.chevron_forward,
              color: Theme.of(context).primaryColor,
            ),
          ],
        ),
      ),
    );
  }
}
