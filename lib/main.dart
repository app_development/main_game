import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:trinkspiel/UI/theme/theme_manager.dart';
import 'package:trinkspiel/controller/shared_preferences_controller.dart';

import 'package:trinkspiel/l10n/app_translation.dart';
import 'package:trinkspiel/l10n/localization_controller.dart';

import 'UI/screens/starting_screen.dart';

import 'controller/app_init.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  bool eraseEverythingFromSP = false;
  await appInit(eraseEverythingFromSP);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeManager().getThemeFromIndex(
            SharedPreferencesController.getPrefs().getInt("currTheme")!),
        themeMode: ThemeMode.light,
        translations: AppTranslation(),
        locale: LocalizationController.getStartingLocale(),
        fallbackLocale: const Locale('en'),
        home: const StartingScreen());
  }
}
