import 'package:get/get.dart';
import 'package:test/test.dart';
import 'package:trinkspiel/Exceptions/controller_exceptions/card_pre_processing_exceptions.dart';
import 'package:trinkspiel/controller/card_text_pre_processing.dart';
import 'package:trinkspiel/controller/in_game_settings_controller.dart';

import 'package:trinkspiel/controller/player_controller.dart';
import 'package:trinkspiel/model/game_card.dart';
import 'package:trinkspiel/model/player.dart';
import 'package:tuple/tuple.dart';

void main() {
  group("Special Operator resolving", () {
    group("Player replacement", () {
      test("Player getting replaced correctly", () {
        Tuple2<String, List<Player?>> result = specialOperation(
            '\$p1\$',
            [Player(name: "TestP"), Player(name: "TestWrong")],
            PlayerController());
        expect(result.item1, "TestP");
      });

      test("Player getting replaced by new Player out of one", () {
        PlayerController players = PlayerController();
        players.players.add(Player(name: "TestP1"));

        Tuple2<String, List<Player?>> result =
            specialOperation('\$p1\$', [null], players);
        expect(result.item1, "TestP1");
      });

      test("Player getting replaced by new Player out of multiple", () {
        PlayerController players = PlayerController();
        players.players.add(Player(name: "TestP1"));
        players.players.add(Player(name: "TestP2"));
        players.players.add(Player(name: "TestP3"));
        players.players.add(Player(name: "TestP4"));

        Tuple2<String, List<Player?>> result =
            specialOperation('\$p1\$', [null], players);

        try {
          expect(result.item1, "TestP1");
        } catch (e) {
          try {
            expect(result.item1, "TestP2");
          } catch (e) {
            try {
              expect(result.item1, "TestP3");
            } catch (e) {
              expect(result.item1, "TestP4");
            }
          }
        }
      });

      test("Player number too high Exception", () {
        Exception caughtException = Exception();

        try {
          specialOperation(
              '\$p2\$', [Player(name: "TestP")], PlayerController());
        } catch (e) {
          caughtException = e as Exception;
        }

        expect(caughtException,
            isA<PlayerNumberHigherThanPlayersInGameException>());
      });

      test("Player number not too high, no Exception", () {
        Exception caughtException = Exception();

        try {
          specialOperation(
              '\$p1\$', [Player(name: "TestP")], PlayerController());
        } catch (e) {
          caughtException = e as Exception;
        }

        expect(caughtException, isA<Exception>());
      });
    });

    group("Special Operator /", () {
      test("Standard case two options", () {
        Tuple2<String, List<Player?>> result =
            specialOperation('\$/Links/Rechts\$', [], PlayerController());

        try {
          expect(result.item1, "Links");
        } catch (e) {
          expect(result.item1, "Rechts");
        }
      });

      test("Standard case five options", () {
        Tuple2<String, List<Player?>> result = specialOperation(
            '\$/Links/Rechts/Oben/Unten/Daneben\$', [], PlayerController());

        try {
          expect(result.item1, "Links");
        } catch (e) {
          try {
            expect(result.item1, "Rechts");
          } catch (e) {
            try {
              expect(result.item1, "Oben");
            } catch (e) {
              try {
                expect(result.item1, "Unten");
              } catch (e) {
                expect(result.item1, "Daneben");
              }
            }
          }
        }
      });

      test("Standard case without randomness", () {
        Tuple2<String, List<Player?>> result =
            specialOperation('\$/Links\$', [], PlayerController());

        expect(result.item1, "Links");
      });

      test("No option given empty string", () {
        Tuple2<String, List<Player?>> result =
            specialOperation('\$/\$', [], PlayerController());

        expect(result.item1, "");
      });
    });

    group("Special Operator s", () {
      test("Standard case soft", () {
        InGameSettingsController inGameSettingsController =
            Get.put(InGameSettingsController());
        inGameSettingsController.setInGameDifficulty(InGameDifficulty.soft);
        Tuple2<String, List<Player?>> result =
            specialOperation('\$s1\$', [], PlayerController());

        expect(result.item1, "1");
      });
      test("Standard case medium", () {
        InGameSettingsController inGameSettingsController =
            Get.put(InGameSettingsController());
        inGameSettingsController.setInGameDifficulty(InGameDifficulty.medium);
        Tuple2<String, List<Player?>> result =
            specialOperation('\$s1\$', [], PlayerController());

        expect(result.item1, "2");
      });

      test("Standard case hard", () {
        InGameSettingsController inGameSettingsController =
            Get.put(InGameSettingsController());
        inGameSettingsController.setInGameDifficulty(InGameDifficulty.hard);
        Tuple2<String, List<Player?>> result =
            specialOperation('\$s1\$', [], PlayerController());

        expect(result.item1, "3");
      });

      test("Standard case extrem", () {
        InGameSettingsController inGameSettingsController =
            Get.put(InGameSettingsController());
        inGameSettingsController.setInGameDifficulty(InGameDifficulty.extrem);
        Tuple2<String, List<Player?>> result =
            specialOperation('\$s1\$', [], PlayerController());

        expect(result.item1, "5");
      });

      test("Empty behind s -> Exception expected", () {
        InGameSettingsController inGameSettingsController =
            Get.put(InGameSettingsController());
        inGameSettingsController.setInGameDifficulty(InGameDifficulty.soft);

        Exception caughtException = Exception();

        try {
          specialOperation('\$s\$', [], PlayerController());
        } catch (e) {
          caughtException = e as Exception;
        }

        expect(caughtException, isA<EmptyBehindSpecialOperatorS>());
      });
    });

    group("Complete Card Pre Processing Testing", () {
      test("Standard case", () {
        PlayerController playerController = Get.put(PlayerController());
        playerController.players.add(Player(name: "TestP1"));

        InGameSettingsController inGameSettingsController =
            Get.put(InGameSettingsController());
        inGameSettingsController.setInGameDifficulty(InGameDifficulty.soft);
        var result = cardPreProcessing(
            "einen Dreier mit \$p1\$ haben?\$-\$Trinke \$s1\$ Schlücke",
            CardType.who_would_rather);

        expect(result, [
          "who_would_rather einen Dreier mit TestP1 haben?",
          "Trinke 1 Schlücke"
        ]);
      });
    });
  });
}
